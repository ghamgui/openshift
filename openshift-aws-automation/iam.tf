ource "aws_iam_role" "ec2_role_automate" {
  name = "ec2_role_automate"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
resource "aws_iam_instance_profile" "ec2_profile_automate" {
  name = "ec2_profile_automate"
  roles = ["ec2_role_automate"]
}
resource "aws_iam_role_policy" "ec2_policy_automate" {
  name = "ec2_policy_automate"
  role = "${aws_iam_role.ec2_role_automate.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:*",
               
        "s3:*",
        
"autoscaling:CreateAutoScalingGroup",
       
        "autoscaling:DeleteAutoScalingGroup",
        
"autoscaling:Describe*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
