sters ASG VARS
echo "terraform asg masters name"
read asg_master
echo "asg masters instance type"
read asg_master_instance_type
echo -n "Please press enter if you want to create a separate storage for docker(yes or no, default is yes): "
read check_docker_masters
check_docker_masters="${check_docker_masters:=yes}"
size_docker_storage_masters=0
if [ "$check_docker_masters" = "yes" ];
then
echo "size of docker storage masters"
read size_docker_storage_masters
fi;
echo "The min number of master instances for the ASG , default is 3)"
read min_of_instance_master
min_of_instance_master="${min_of_instance_master:=3}"
echo "The max number of master instances for the ASG and it must be odd, default is 3)"
read max_of_instance_master
max_of_instance_master="${max_of_instance_master:=3}"
while [ $(( $max_of_instance_master % 2 )) -eq 0 ]
do
 read max_of_instance_master
done
######### Nodes ASG VARS
echo "terraform asg nodes name"
read asg_nodes
echo "asg nodes instance type"
read asg_nodes_instance_type
echo -n "Please press enter if you want to create a separate storage for docker(yes or no, default is yes): "
read check_docker_nodes
check_docker_nodes="${check_docker_nodes:=yes}"
size_docker_storage_nodes=0
if [ "$check_docker_nodes" = "yes" ];
then
echo "size of docker storage "
read size_docker_storage_nodes
fi;
echo "The min number of nodes instances for the ASG , default is 3)"
read min_of_instance_nodes
min_of_instance_nodes="${min_of_instance_nodes:=3}"
echo "The max number of nodes instances for the ASG , default is 4)"
read max_of_instance_nodes
max_of_instance_nodes="${max_of_instance_nodes:=4}"
########## The third party VARS
echo "terraform third party asg name (infra or etcd, default is infra):"
read asg_third_party
asg_third_party="${asg_third_party:=infra}"
echo "asg third party name"
read asg_third_party_name
echo "asg third party instance type"
read asg_third_instance_type
echo -n "Please press enter if you want to create a separate storage for docker(yes or no, default is yes): "
read check_docker_third_party
check_docker_third_party="${check_docker_third_party:=yes}"
size_docker_storage_third_party=0
if [ "$check_docker_third_party" = "yes" ];
then
echo "size of docker storage "
read size_docker_storage_third_party
fi;
echo "The min number of third_party instances for the ASG , default is 3)"
read min_of_instance_third_party
min_of_instance_third_party="${min_of_instance_third_party:=3}"
echo "The max number of third_party instances for the ASG , default is 4)"
read max_of_instance_third_party
max_of_instance_third_party="${max_of_instance_third_party:=4}"
########### VPC ID
select ID in $(aws ec2 describe-vpcs --query 'Vpcs[*].[Tags[?Key==`Name`].Value]' --output text); do
        PS3='Select vpc: '
        for  vpcid in $ID; do
        vpcd=$(aws ec2 describe-vpcs --query 'Vpcs[?contains(Tags[?Key==`Name`].Value[], `'$ID'`) == `true`].[VpcId]' --output text)
        vpcpublicsubnets=$(aws ec2 describe-subnets --filters "Name=vpc-id,Values=$vpcd","Name=tag:Name,Values=Public-1*" --query 'Subnets[*].{ID:SubnetId}'  --output text)
        vpcprivatesubnets=$(aws ec2 describe-subnets --filters "Name=vpc-id,Values=$vpcd","Name=tag:Name,Values=Private-1*" --query 'Subnets[*].{ID:SubnetId}'  --output text)
        az=$(aws ec2 describe-availability-zones --region eu-west-1 --query 'AvailabilityZones[*].{Name:ZoneName}' --output text)
                if [ $ID == 0 ]
                        then
                                break
                fi
                        echo -e "\n the vpc id is" $vpcd >&2
                        #echo -e "\n these are the public subnets \n $vpcpublicsubnets" >&2
                        #echo -e "\n these are the private subnets \n $vpcprivatesubnets" >&2
                        #echo -e "\n\v  $az" >&2
                                break
        done;
        break;
done;
i=0
vpcd_public_subnets='['
for vpcpublic in $vpcpublicsubnets; do
        (( i++ ));
        vpcd_public_subnets+='"'$vpcpublic'",'
        cat <<EOT >> ./variable.tf
variable "vpc_public_$i" {
        default     = "$vpcpublic"
        description = "These are the public subnets"
        }
EOT
done;
vpcd_public_subnets=$(echo -n $vpcd_public_subnets | head -c -1)
vpcd_public_subnets+="]"
echo $vpcd_public_subnets
j=0
vpcd_private_subnets='['
for vpcprivate in $vpcprivatesubnets; do
        ((j++));
        vpcd_private_subnets+='"'$vpcprivate'",'
        cat <<EOT >> ./variable.tf
variable "vpc_private_$j" {
        default     = "$vpcprivate"
        description = "These are the public subnets"
        }
EOT
done;
vpcd_private_subnets=$(echo -n $vpcd_private_subnets | head -c -1)
vpcd_private_subnets+="]"
echo $vpcd_private_subnets
####### SSH KEY
echo "Your ssh key name"
read ssh_key_name
echo "Your ssh key path"
read ssh_key_path
########### ALB CONFIG
echo "ALB name"
read ALB_name
if [[ "$asg_third_party" = "etcd" ]]
then
ALB_attachment=$asg_master
elif [[ "$asg_third_party" = "infra" ]]
then
echo -n "Please choose the ASG for your ALB Attachment(infra or master, default is infra): "
read attach
attach="${attach:=infra}"
   if  [[ "$attach" != "master" ]]
   then
   ALB_attachment=$asg_third_party_name
   else
   ALB_attachment=$asg_master
   fi
fi
########## AWS Region
aws_region=$(cat .aws/config | grep region | cut -b 10-30)
echo $aws_region
########## export
export vpcd_public_subnets="$vpcd_public_subnets" \
export vpcd_private_subnets="$vpcd_private_subnets" \
export aws_region="$aws_region" \
export ALB_name="$ALB_name" \
export ALB_attachment="$ALB_attachment" \
export ssh_key_name="$ssh_key_name" \
export ssh_key_path="$ssh_key_path" \
export vpcd="$vpcd" \
export asg_third_party_name="$asg_third_party_name" \
export max_of_instance_master="$max_of_instance_master" \
export min_of_instance_master="$min_of_instance_master" \
export min_of_instance_nodes="$min_of_instance_nodes" \
export max_of_instance_nodes="$max_of_instance_nodes" \
export min_of_instance_third_party="$min_of_instance_third_party" \
export max_of_instance_third_party="$max_of_instance_third_party" \
export asg_master="$asg_master" \
export asg_master_instance_type="$asg_master_instance_type" \
export size_docker_storage_masters="$size_docker_storage_masters" \
export asg_nodes="$asg_nodes" \
export asg_nodes_instance_type="$asg_nodes_instance_type" \
export size_docker_storage_nodes="$size_docker_storage_nodes" \
export asg_third_party="$asg_third_party" \
export asg_third_instance_type="$asg_third_instance_type" \
export size_docker_storage_third_party="$size_docker_storage_third_party" && envsubst < var_template.tf > variable.tf
########### VPC SUBNETS
i=0
for vpcpublic in $vpcpublicsubnets; do
        (( i++ ));
        cat <<EOT >> ./variable.tf
variable "vpc_public_$i" {
        default     = "$vpcpublic"
        description = "These are the public subnets"
        }
EOT
done;
j=0
for vpcprivate in $vpcprivatesubnets; do
        ((j++));
        cat <<EOT >> ./variable.tf
variable "vpc_private_$j" {
        default     = "$vpcprivate"
        description = "These are the public subnets"
        }
EOT
done;
