" {
  region       = "${var.aws_region}"
  shared_credentials_file = "~/.aws/credentials"
}
########## SESCURITY GROUPS MASTERS
resource "aws_security_group" "master_public" {
  name = "${var.asg_master}-master-public"
  description = "Master public group for ${var.asg_master}"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8443
    to_port = 8443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 53
    to_port = 53
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 53
    to_port = 53
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port = 8053
    to_port = 8053
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8053
    to_port = 8053
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port = 2379
    to_port = 2379
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 2380
    to_port = 2380
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tag {
    key = "kubernetes.io/cluster/${var.ClusterID}"
    value = "owned"
    propagate_at_launch = true
  }
  vpc_id = "${var.vpcd}"
}
########### SESCURITY GROUPS NODES
resource "aws_security_group" "node" {
  name = "${var.asg_nodes}-node"
  description = "Cluster node group for ${var.asg_nodes}"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_groups = ["${aws_security_group.bastion.id}"]
  }
  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    self = true
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
 tag {
    key = "kubernetes.io/cluster/${var.ClusterID}"
    value = "owned"
    propagate_at_launch = true
  }
  vpc_id = "${var.vpcd}"
}
########## SECURITY GROUP INFRA
resource "aws_security_group" "infra_node" {
  name = "${var.asg_third_party_name}-infra-node"
  description = "Infra node group for ${var.asg_third_party_name}"
  
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port = 2379
    to_port = 2379
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 2380
    to_port = 2380
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tag {
    key = "kubernetes.io/cluster/${var.ClusterID}"
    value = "owned"
    propagate_at_launch = true
  }
  vpc_id = "${var.vpcd}"
}
############ 
resource "aws_security_group" "bastion" {
  name = "${var.Bastion_name}-bastion"
  description = "Bastion group for ${var.Bastion_name}"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks  = ["0.0.0.0/0"]
  }
  tag {
    key = "kubernetes.io/cluster/${var.ClusterID}"
    value = "owned"
    propagate_at_launch = true
  }
  vpc_id = "${var.vpcd}"
}
######### ASG masters
resource "aws_launch_configuration" "terraform-openshift-masters-automate" {
 key_name               = "${aws_key_pair.generated_ssh_key.key_name}"
 image_id               = "${var.ami}"
 iam_instance_profile   = "${aws_iam_instance_profile.ec2_profile.name}"
 instance_type          = "${var.asg_master_instance_type}"
 security_groups = [
    "${aws_security_group.node.id}",
    "${aws_security_group.master_public.id}"
 ]
 lifecycle {
   create_before_destroy = true
 }
 root_block_device {
    volume_type = "gp2"
    volume_size = 80
 }
 ebs_block_device {
      device_name = "/dev/sdb"
      volume_size = "${var.size_docker_storage_masters}"
      volume_type = "gp2"
      delete_on_termination = true
  }
  tag {
    key = "kubernetes.io/cluster/${var.ClusterID}"
    value = "owned"
    propagate_at_launch = true
  }
}
resource "aws_autoscaling_group" "${var.asg_master}" {
 launch_configuration = "${aws_launch_configuration.terraform-openshift-masters-automate.id}"
 name                 = "Openshift Master"
 vpc_zone_identifier  = "${var.vpcd_private_subnets}"
 availability_zones   = "${var.az}"
 min_size = "${var.min_of_instance_master}"
 max_size = "${var.max_of_instance_master}"
 tag {
    key = "kubernetes.io/cluster/${var.ClusterID}"
    value = "owned"
    propagate_at_launch = true
  }
 tag {
   key = "Name"
   value = "terraform-asg-masters"
   propagate_at_launch = true
 }
}
################ ASG nodes
resource "aws_launch_configuration" "terraform-openshift-nodes-automate" {
 key_name               = "${aws_key_pair.generated_ssh_key.key_name}"
 image_id               = "${var.ami}"
 iam_instance_profile   = "${aws_iam_instance_profile.ec2_profile.name}"
 instance_type          = "${var.asg_nodes_instance_type}"
 security_groups = [
    "${aws_security_group.node.id}"
 ]
 lifecycle {
   create_before_destroy = true
 }
 root_block_device {
    volume_type = "gp2"
    volume_size = 80
 }
 ebs_block_device {
      device_name = "/dev/sdb"
      volume_size = "${var.size_docker_storage_nodes}"
      volume_type = "gp2"
      delete_on_termination = true
 }
 tag {
    key = "kubernetes.io/cluster/${var.ClusterID}"
    value = "owned"
    propagate_at_launch = true
  }
}
resource "aws_autoscaling_group" "${var.asg_nodes}" {
 launch_configuration = "${aws_launch_configuration.terraform-openshift-nodes-automate.id}"
 name                 = "Openshift Node"
 vpc_zone_identifier  = "${var.vpcd_private_subnets}"
 availability_zones   = "${var.az}"
 min_size = "${var.min_of_instance_nodes}"
 max_size = "${var.max_of_instance_nodes}"
 tag {
    key = "kubernetes.io/cluster/${var.ClusterID}"
    value = "owned"
    propagate_at_launch = true
  }
 tag {
   key = "Name"
   value = "terraform-asg-nodes"
   propagate_at_launch = true
 }
}
############ ASG Third party
resource "aws_launch_configuration" "terraform-openshift-third_party-automate" {
 key_name               = "${aws_key_pair.generated_ssh_key.key_name}"
 image_id               = "${var.ami}"
 iam_instance_profile   = "${aws_iam_instance_profile.ec2_profile.name}"
 instance_type          = "${var.asg_third_instance_type}"
 security_groups = [
    "${aws_security_group.node.id}",
    "${aws_security_group.infra_node.id}"
  ]
 lifecycle {
   create_before_destroy = true
 }
 root_block_device {
    volume_type = "gp2"
    volume_size = 80
 }
 ebs_block_device {
      device_name = "/dev/sdb"
      volume_size = "${var.size_docker_storage_third_party}"
      volume_type = "gp2"
      delete_on_termination = true
 }
 tag {
    key = "kubernetes.io/cluster/${var.ClusterID}"
    value = "owned"
    propagate_at_launch = true
  }
}
resource "aws_autoscaling_group" "${var.asg_third_party_name}" {
 launch_configuration = "${aws_launch_configuration.terraform-openshift-third_party-automate.id}"
 name                 = "Openshift Infra"
 vpc_zone_identifier  = "${var.vpcd_private_subnets}"
 availability_zones   = "${var.az}"
 min_size = "${var.min_of_instance_third_party}"
 max_size = "${var.max_of_instance_third_party}"
 tag {
    key = "kubernetes.io/cluster/${var.ClusterID}"
    value = "owned"
    propagate_at_launch = true
  }
 tag {
   key = "Name"
   value = "terraform-asg-infra"
   propagate_at_launch = true
 }
}
######## security alb
resource "aws_security_group" "lb_sg_automate" {
  description = "controls access to the application ELB"
  vpc_id = "${var.vpcd}"
  name   = "alb_security_group_automate"
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
############# alb
resource "aws_alb_target_group" "terraform-openshift-alb-automate" {
  name     = "terraform-openshift-alb-automate"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpcd}"
}
resource "aws_alb" "${var.ALB_name} {
  name            = "oc-terraform-alb-automate"
  internal        = false
  subnets         = "${var.vpcd_public_subnets}"
  security_groups = ["${aws_security_group.lb_sg_automate.id}"]
tag {
    key = "kubernetes.io/cluster/${var.ClusterID}"
    value = "owned"
    propagate_at_launch = true
  }
}
resource "aws_alb_listener" "front-end-automate" {
  load_balancer_arn = "${aws_alb.oc-terraform-alb-automate.id}"
  port              = "80"
  protocol          = "HTTP"
  default_action {
    target_group_arn = "${aws_alb_target_group.terraform-openshift-alb-automate.id}"
    type             = "forward"
  }
}
######## attach alb 
resource "aws_autoscaling_attachment" "asg_attachment_automate" {
 autoscaling_group_name = "${aws_autoscaling_group.${var.asg_master}.id}"
 alb_target_group_arn   = "${aws_alb_target_group.terraform-openshift-alb-automate.arn}"
}
################ keyyyyy
resource "tls_private_key" "generated_ssh_key" {
 algorithm = "RSA"
 rsa_bits  = 4096
}
resource "aws_key_pair" "generated_ssh_key" {
 key_name   =  "${var.ssh_key_name}"
 depends_on = ["tls_private_key.generated_ssh_key"]
 public_key = "${tls_private_key.generated_ssh_key.public_key_openssh}"
}
#Storing the key pair via local file
resource "local_file" "private_ssh_key_pem" {
 content    = "${tls_private_key.generated_ssh_key.private_key_pem}"
 depends_on = ["tls_private_key.generated_ssh_key"]
 filename   =  "${var.ssh_key_path}"
 provisioner "local-exec" {
   command = "chmod a+rwx ${var.ssh_key_path}"
 }
}
